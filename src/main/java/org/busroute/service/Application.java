/*
 * Copyright (c) 2016, WSO2 Inc. (http://wso2.com) All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.busroute.service;

import org.wso2.msf4j.MicroservicesRunner;

import java.io.IOException;

/**
 * Application entry point.
 *
 * @since 1.0-SNAPSHOT
 */
public class Application {
    public static void main(String[] args) throws IOException {
        String pathArg = (args.length > 0)? args[0] : null;

        if(pathArg == null){
            throw new IllegalArgumentException("Please supply a valid path parameter!");
        }
        IRouteDataSource source = new RouteFileDataSource(pathArg);
        IRouteManager manager = new RouteManagerBaseImpl();
        source.loadRoutes(manager);
        new MicroservicesRunner(8088)
                .deploy(new DirectRouteService(manager))
                .start();
    }
}
