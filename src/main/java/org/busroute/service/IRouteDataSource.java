package org.busroute.service;

import java.io.IOException;

/**
 * Created by psherif on 14/01/2017.
 */
public interface IRouteDataSource {
    public void loadRoutes(IRouteManager routeManager) throws IOException;
}
