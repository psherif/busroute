package org.busroute.service;

import java.util.*;

/**
 * Created by psherif on 14/01/2017.
 */
public class RouteManagerBaseImpl implements IRouteManager {

    Map<Integer, SortedSet<Integer>> stationMap =  new HashMap<>();

    @Override
    public void addRoute(int routeId, int[] stationIds) {
        for (int station:stationIds) {
            SortedSet<Integer> connectedRoutes = stationMap.get(station);
            if(connectedRoutes == null){
                connectedRoutes = new TreeSet<>();
                stationMap.put(station, connectedRoutes);
            }
            connectedRoutes.add(routeId);
        }

    }

    @Override
    public boolean hasDirectConnection(int depStationId, int arrStationId) {
        int connection = findConnection(depStationId,arrStationId);
        return connection > 0;

    }

    @Override
    public int[] connectionList(int sid) {
        SortedSet<Integer> connections = stationMap.get(sid);
        if(connections == null) return new int[0];

        int[] result = new int[connections.size()];
        int index = 0;
        for (int connection:connections
             ) {
            result[index]=connection;
            index++;
        }
        return result;
    }

    @Override
    public int findConnection(int depStationId, int arrStationId) {

        SortedSet<Integer> connectedRoutesDep  = stationMap.get(depStationId);
        SortedSet<Integer> connectedRoutesArr  = stationMap.get(arrStationId);
        if(connectedRoutesDep == null || connectedRoutesDep.size() == 0 || connectedRoutesArr == null || connectedRoutesArr.size() == 0){
            return -1;
        }

        Iterator<Integer> itDep = connectedRoutesDep.iterator();

        Iterator<Integer> itArr = connectedRoutesArr.iterator();

        int routeDep = itDep.next();
        int routeArr =  itArr.next();

        if(routeDep == routeArr) return routeDep;

        while(itDep.hasNext()){
            routeDep = itDep.next();
            if(routeDep == routeArr) return routeDep;

            while(routeDep > routeArr){
                if(!itArr.hasNext()){
                    return -1;
                }
                routeArr = itArr.next();
                if(routeDep == routeArr) return routeDep;

            }
        }
        return -1;
    }
}
