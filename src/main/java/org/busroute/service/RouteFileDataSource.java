package org.busroute.service;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.IllegalFormatException;
import java.util.stream.Stream;

/**
 * Created by psherif on 14/01/2017.
 */
public class RouteFileDataSource implements IRouteDataSource {

    String filePath;

    public RouteFileDataSource(String filePath) {
        this.filePath = filePath;
    }

    @Override
    public void loadRoutes(IRouteManager routeManager) throws IOException {

        try(BufferedReader br=Files.newBufferedReader(Paths.get(this.filePath))) {
            String firstLine = br.readLine();
            if(firstLine==null) throw new IOException("Empty file!");
            String[] parts = firstLine.split("\\s+");
            if(parts.length != 1){
                throw new IOException("Wrong File Format!");
            }
            int numRoutes = Integer.parseInt(parts[0]);
            if(numRoutes > 100000){
                throw  new IOException("No more than 100,000 routes allowed!");
            }

            String line = br.readLine();
            int routesRead = 0;
            while(line != null && numRoutes > 0){
                routesRead++;
                String[] routeParts = line.split("\\s+");
                if(routeParts.length < 3 || routeParts.length > 1000){
                    throw  new IOException("Invalid route format for route: "+routesRead+"!");
                }
                int routeId = Integer.parseInt(routeParts[0]);
                int[] stations = new int[routeParts.length -1];
                for(int i = 0; i < stations.length;i++){
                    stations[i] = Integer.parseInt(routeParts[i+1]);
                }
                routeManager.addRoute(routeId, stations);
                line = br.readLine();
            }

        }
    }
}
