/*
 * Copyright (c) 2016, WSO2 Inc. (http://wso2.com) All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.busroute.service;

import com.google.common.collect.Maps;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.Arrays;
import java.util.Map;

/**
 * This is the Microservice resource class.
 * See <a href="https://github.com/wso2/msf4j#getting-started">https://github.com/wso2/msf4j#getting-started</a>
 * for the usage of annotations.
 *
 * @since 1.0-SNAPSHOT
 */
@Path("/api")
public class DirectRouteService {

    IRouteManager routeManager;

    public DirectRouteService(IRouteManager routeManager) {
        this.routeManager = routeManager;
    }

    @GET
    @Path("/direct")
    @Produces(MediaType.APPLICATION_JSON)
    public Response hasConnection(@QueryParam("dep_sid") int dep, @QueryParam("arr_sid") int arr) {

        boolean directConnection = routeManager.hasDirectConnection(dep, arr);

        Map<String, Object> entity = Maps.newHashMap();
        entity.put("dep_sid",dep);
        entity.put("arr_sid", arr);
        entity.put("direct_bus_route", directConnection);

        return Response.status(Response.Status.OK).entity(entity).build();
    }

    @GET
    @Path("/connections")
    public String getConnections(@QueryParam("sid") int sid) {
        int[] connections = routeManager.connectionList(sid);
        return Arrays.toString(connections);
    }
}
