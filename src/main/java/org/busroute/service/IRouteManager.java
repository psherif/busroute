package org.busroute.service;

/**
 * Created by psherif on 14/01/2017.
 */
public interface IRouteManager {
    public void addRoute(int routeId, int[] stationIds);
    public boolean hasDirectConnection(int depStationId, int arrStationId);
    public int[] connectionList(int sid);
    public int findConnection(int depStationId, int arrStationId);
}
