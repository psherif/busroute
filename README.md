**My Solution for the Bus Route Challenge**

Using the WSO2 Microservices Framework for Java: 
http://wso2.com/products/microservices-framework-for-java/

Build by executing _build.sh_
Start using _service.sh start [path_to_busroute_datafile]_.
_service.sh stop_ to stop.